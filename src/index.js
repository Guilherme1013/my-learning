import ReactDOM from 'react-dom'

import './index.css'

import App from "./lesson-1-custom-hooks/App"


ReactDOM.render(
    <App />,
    document.getElementById('root')
)

