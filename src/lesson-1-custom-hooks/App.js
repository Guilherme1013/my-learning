import { Fragment, useEffect, useState } from "react"

import useHttp from "./useHttp"

const firebaseUrl = "https://customhooks-6a88a-default-rtdb.firebaseio.com/users.json"

const App = () => {
    const [users, setUsers] = useState([])

    const createdUser = user => {
        setUsers(prev => ({user, ...prev}))
    }

    return (
        <Fragment>
            <NewUser createdUser={createdUser} />
            <ShowUsers users={users} />
        </Fragment>
    )
}

export default App


const NewUser = ({createdUser}) => {
    const {
        sendRequest: sendUser,
        isLoading
    } = useHttp()
    
    const sendUserHandler = (username) => {
        sendUser({
            url: firebaseUrl,
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: {name: username}
        }, data => {
            const generatedId = data.name
            const user = {id: generatedId, name: data[generatedId].name}
            createdUser(prev => ({user, ...prev}))
        })
    }

    return <Form sendUserHandler={sendUserHandler} isLoading={isLoading} />
}

const Form = ({isLoading, sendUserHandler}) => {
    const [username, setUsername] = useState("")

    const inputChangeHandler = e => {
        setUsername(e.target.value)
    }

    const submitHandler = e => {
        e.preventDefault()
        sendUserHandler(username)
        setUsername("")
    }

    return (
        <form onSubmit={submitHandler}>
            <input onChange={inputChangeHandler} value={username} type="text" />
            <button type="submit">{!isLoading ? "Submit" : "Sending..."}</button>
        </form>
    )
}

const ShowUsers = () => {
    const {
        sendRequest: fetchUsers,
        error,
        isLoading
    } = useHttp()
    const [users, setUsers] = useState([])

    useEffect(() => {
        fetchUsers(
            {url: firebaseUrl},
            userObj => {
                const loadedUsers = []

                for(const userKey in userObj) {
                    loadedUsers.push({id: userKey, name: userObj[userKey].name})
                }

                setUsers(loadedUsers)
            }
        )
    }, [fetchUsers])

    if(isLoading) {
        return <h1>Loading...</h1>
    }

    if(error) {
        return <p>{error}</p>
    }

    return (
        <ul>
            {users.map(user => <li key={user.id}>{user.name}</li>)}
        </ul>
    )
}

