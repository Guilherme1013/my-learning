import { useState, useCallback } from "react"

const useHttp = () => {
    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState(null)

    const sendRequest = useCallback((requestConfig, applyData) => {
        setIsLoading(true)

        fetch(requestConfig.url, {
            method: requestConfig.method ? requestConfig.method : "GET",
            headers: requestConfig.headers ? requestConfig.headers : {},
            body: requestConfig.body ? JSON.stringify(requestConfig.body) : null,
        })
            .then(res => {
                if(!res.ok) {
                    throw new Error("Request failed!")
                }
                return res.json()
            })
            .then(data => {
                applyData(data)
                setIsLoading(false)
            })
            .catch(error => setError(error.message || "Something went wrong!"))
    }, [])

    return {
        sendRequest,
        error,
        isLoading
    }
}

export default useHttp

